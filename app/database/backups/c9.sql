-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2015 a las 17:45:00
-- Versión del servidor: 5.5.40-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `c9`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `commenter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=172 ;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `commenter`, `email`, `comment`, `approved`, `created_at`, `updated_at`) VALUES
(1, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(2, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(3, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(4, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(5, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(6, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(7, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(8, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(9, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(10, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(11, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(12, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(13, 1, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(14, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(15, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(16, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(17, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(18, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(19, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(20, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(21, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(22, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(23, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(24, 2, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(25, 3, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(26, 3, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(27, 3, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(28, 3, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(29, 3, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(30, 4, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(31, 4, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(32, 4, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(33, 4, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(34, 4, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(35, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(36, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(37, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(38, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(39, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(40, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(41, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(42, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(43, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(44, 5, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(45, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(46, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(47, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(48, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(49, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(50, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(51, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(52, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(53, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(54, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(55, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(56, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(57, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(58, 6, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(59, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(60, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(61, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(62, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(63, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(64, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(65, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(66, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(67, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(68, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(69, 7, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(70, 8, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(71, 8, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(72, 8, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(73, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(74, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(75, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(76, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(77, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(78, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(79, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(80, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(81, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(82, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(83, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(84, 9, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(85, 10, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(86, 10, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(87, 10, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(88, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(89, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(90, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(91, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(92, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(93, 11, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(94, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(95, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(96, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(97, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(98, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(99, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(100, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(101, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(102, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(103, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(104, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(105, 12, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(106, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(107, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(108, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(109, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(110, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(111, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(112, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(113, 13, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(114, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(115, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(116, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(117, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(118, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(119, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(120, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(121, 14, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(122, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(123, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(124, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(125, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(126, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(127, 15, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(128, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(129, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(130, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(131, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(132, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(133, 16, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(134, 17, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(135, 17, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(136, 17, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(137, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(138, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(139, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(140, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(141, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(142, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(143, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(144, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(145, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(146, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(147, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(148, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(149, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(150, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(151, 18, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(152, 19, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(153, 19, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(154, 19, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(155, 19, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(156, 19, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(157, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(158, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(159, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(160, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(161, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(162, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(163, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(164, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(165, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(166, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(167, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(168, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(169, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(170, 20, 'xyz', 'xyz@xmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 0, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(171, 21, 'Blas', 'blas@trapagual.com', 'Sí que parece bonito el POST', 1, '2015-01-20 17:42:24', '2015-01-20 17:42:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_20_123045_crear_tabla_posts', 1),
('2015_01_20_123241_crear_tabla_comments', 1),
('2015_01_20_163449_crear_tabla_usuarios', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read_more` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_count` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `search` (`title`,`content`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `title`, `read_more`, `content`, `comment_count`, `created_at`, `updated_at`) VALUES
(1, 'Post no 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 13, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(2, 'Post no 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 11, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(3, 'Post no 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 5, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(4, 'Post no 4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 5, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(5, 'Post no 5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 10, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(6, 'Post no 6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 14, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(7, 'Post no 7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 11, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(8, 'Post no 8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 3, '2015-01-20 17:40:24', '2015-01-20 17:40:24'),
(9, 'Post no 9', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 12, '2015-01-20 17:40:24', '2015-01-20 17:40:25'),
(10, 'Post no 10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 3, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(11, 'Post no 11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 6, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(12, 'Post no 12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 12, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(13, 'Post no 13', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 8, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(14, 'Post no 14', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 8, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(15, 'Post no 15', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 6, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(16, 'Post no 16', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 6, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(17, 'Post no 17', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 3, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(18, 'Post no 18', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 15, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(19, 'Post no 19', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 5, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(20, 'Post no 20', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n                    Praesent vel ligula scelerisque, vehicula dui eu, fermentum velit. \n                    Phasellus ac ornare eros, quis malesuada augue. Nunc ac nibh at mauris dapibus fermentum. \n                    In in aliquet nisi, ut scelerisque arcu. Integer tempor, nunc ac lacinia cursus, \n                    mauris justo volutpat elit, eget accumsan nulla nisi ut nisi. Etiam non convallis ligula. Nulla urna augue, \n                    dignissim ac semper in, ornare ac mauris. Duis nec felis mauris.', 14, '2015-01-20 17:40:25', '2015-01-20 17:40:25'),
(21, 'Post Bonito', 'Este post es muy bonito', 'Este post es muy bonito', 1, '2015-01-20 17:41:48', '2015-01-20 17:42:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'usm4n', '$2y$10$OM775BaKCEo6/jMP7RfuKekD5acx9Y87IduY0kWZBvWtW2e2GqFoy', '', 'YNwtCFeGZ8y9u6wzgnt9Bvx7WAaq7rfTdaR2Vh4UrOG7FwVPrrRakf6j7A0o', '2015-01-20 17:40:03', '2015-01-20 17:42:57');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

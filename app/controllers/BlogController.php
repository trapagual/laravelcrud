<?php

class BlogController extends BaseController {
    
    public function __construct() {
        // para impedir que un logado vuelva a logarse
        $this->beforeFilter('guest',['only' => ['getLogin']]);
        $this->beforeFilter('auth',['only' => ['getLogout']]);
    }
    
    public function getIndex() {
        $posts = Post::orderBy('id', 'desc')->paginate(10);
        $posts->getFactory()->setViewName('pagination::simple');
        $this->layout->title='Principal | Blog con Laravel 4';
        $this->layout->main=View::make('home')->nest('content', 'index', compact('posts'));
    }
    
    public function getSearch() {
        $searchTerm = Input::get('s');
        $post = Post::whereRaw('match(title, content) against(? in boolean mode', [$searchTerm])->paginate(10);
        $posts->getFactory()->setViewName('pagination::slider');
        $posts->appends(['s'=>$searchTerm]);
        $this->layout->with('title', 'Search: '.$searchTerm);
        $this->layout->main = View::make('home')->nest('content', 'index', ($post->isEmpty()) ? ['notFound'=>true] : compact('posts'));
    }
    
    public function getLogin() {
        $this->layout->title = 'login';
        $this->layout->main = View::make('login');
    }
    
    public function postLogin() {
        $credentials = [
            'username'=>Input::get('username'),
            'password'=>Input::get('password')
        ];
        
        $rules = [
            'username'=>'required',
            'password'=>'required'
        ];
        
        $validator = Validator::make($credentials, $rules);
        if($validator->passes()) {
            if(Auth::attempt($credentials))
                return Redirect::to('admin/dash-board');
            // else
            return Redirect::back()->withInput()->with('failure','usuario o clave no válidos');
        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }

    }
    
    public function getLogout() {
        Auth::logout();
        return Redirect::to('/');
    }
}

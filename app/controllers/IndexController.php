<?php

class IndexController extends BaseController {
    
    public function showIndex() {
        // genera la respuesta para index.blade.php
        return View::make('index');
        // hay que registrar su ruta en route.php como
        // Route::get('index', 'IndexController@showIndex');
    }
}